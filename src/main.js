import { createApp } from 'vue'
import App from './App2.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import axios from 'axios'
import router from './router/router.js'
import store from './store/store.js'

const app = createApp(App)
app.config.globalProperties.$axios=axios
app.use(router)
app.use(store)
app.use(ElementPlus)
app.mount('#app')

