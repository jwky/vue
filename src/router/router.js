import {createRouter,createWebHistory} from 'vue-router'
import School from '../components/School.vue'
import userlist from '../components/userlist.vue'
import home from '../components/home.vue'
import Right from '../components/Right.vue'
import MediaNews from '../components/MediaNews.vue'
import HotNews from '../components/HotNews.vue'
import News from '../components/News.vue'
import NewsDetail from '../components/NewsDetail.vue'

const routes =[
	{
		path:'/school',
		component:School
	},
	{
		path:'/',
		component:'/home'
	},
	{
		psth:'/Right',
		component:Right
	},
	{
		path:'/home',
		component:home
	},
	{
		path:'/userlist',
		component:userlist
	},
	{
		path:'/news',
		component:News,
		children:[
			{
				path:'HotNews',
				name:'HotNews',
				component:HotNews
			},
			{
				path:'MediaNews',
				name:'MediaNews',
				component:MediaNews
			},
		]
	},
	{
		path:'/newsdetail/:id',
		component:NewsDetail
	},
]

const router =createRouter({
	history:createWebHistory(),
	routes
})
export default router;