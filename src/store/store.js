import {createStore} from 'vuex'

const store = createStore({
	state(){
		return{
			name:'张三',
			age:20,
			count:1,
		}
	},
	mutations:{
		add(state,num){
			state.count+=num
		},
		reduce(state,num){
			state.count-=num
		}
	}
})

export default store;